<?php
namespace api\controllers;

use yii\rest\ActiveController;
use yii;
use sizeg\jwt\JwtHttpBearerAuth;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use common\models\Message;

class MessageController extends ActiveController
{
  public $modelClass = 'common\models\Message';
	public function behaviors()
	{
		$behaviors = parent::behaviors();
                $behaviors['corsFilter'] = [
                        'class' => \yii\filters\Cors::className(),
                    'cors' => [
                        'Origin' => ['*'],
                        'Access-Control-Request-Method' => ['GET', 'POST', 'DELETE', 'OPTIONS'],
                        'Access-Control-Request-Headers' => ['*'],
                        'Access-Control-Allow-Credentials' => null,
                        'Access-Control-Max-Age' => 86400,
                        'Access-Control-Expose-Headers' => [
                                'X-Pagination-Current-Page',
                                'X-Pagination-Per-Page',
                                'X-Pagination-Page-Count',
                                'X-Pagination-Total-Count',
                                ]
                        ],
                ];
                $behaviors['authenticator'] = [
                    'class' => JwtHttpBearerAuth::class,
                    'auth' => function ($token, $authMethod) {
                        $user = \common\models\User::findOne($token->getClaim('uid'));
                        if($user)
                        {
                          Yii::$app->user->setIdentity($user);
                        }
                        return $user;
                     }
                ];
		return $behaviors;
	}

  public function actions()
	{
		$actions = parent::actions();
		$actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];
		return $actions;
	}

  public function prepareDataProvider()
	{
		  $user = Yii::$app->user->identity;
      $type = Yii::$app->request->getQueryParam('message_type'); // search query
      $status = Yii::$app->request->getQueryParam('user_status'); // filter

      $query = Message::find();

      if("received" == $type)
      {
        $query->andWhere(['receiver' => $user->id]);
      }
      else if("sent" == $type)
      {
        $query->andWhere(['creator' => $user->id]);
        if("delivered" == $status)
        {
          $query->andWhere(['received_at' => null]);
        }
        else if("received" == $status)
        {
          $query->andWhere(['not', ['received_at' => null]]);
        }
      }
      else
      {
        $query->own();
      }

      $this->markRead($query);

      $provider = new ActiveDataProvider([
          'query' => $query,
          'sort' => [
              'attributes' => ['created_at' => SORT_DESC],
          ],
          'pagination' => [
              'pageSize' => 20,
          ],
      ]);

      return $provider;
	}

  private function markRead($query)
  {
    $user = Yii::$app->user->identity;
    $queryCopy = clone $query;
    $unread = $queryCopy->andWhere(['received_at' => null, 'receiver' => $user->id])->column();
    $unread = array_map('intval', $unread);
    if(is_array($unread) && count($unread) > 0)
    {
      \Yii::$app->db->createCommand()
      ->update("message", ['received_at' => time()], ['id' => $unread])
      ->execute();
    }
  }

}

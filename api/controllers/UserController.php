<?php
namespace api\controllers;

use Yii;
use common\models\LoginForm;
use common\models\ResetPasswordForm;
use common\models\SignupForm;
use common\models\GenericUser;
use common\models\PasswordResetRequestForm;
use yii\filters\ContentNegotiator;
use yii\web\Response;
use yii\web\BadRequestHttpException;
use yii\rest\Controller;
use Lcobucci\JWT\Signer\Hmac\Sha256;
/**
 * Site controller
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];

		$behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
        ];

        return $behaviors;

    }

    public function actionLogin()
    {
	     if(Yii::$app->request->getIsOptions()) return "OK";
        $model = new LoginForm();

        $signer = new Sha256();

        if ($model->load(Yii::$app->getRequest()->getBodyParams(), '') && $model->login()) {
          $token = Yii::$app->jwt->getBuilder()
            ->setIssuer('http://api.localhost') // Configures the issuer (iss claim)
            ->setAudience('http://api.localhost') // Configures the audience (aud claim)
            ->setId(Yii::$app->user->identity->id, true) // Configures the id (jti claim), replicating as a header item
            ->setIssuedAt(time()) // Configures the time that the token was issue (iat claim)
            ->setNotBefore(time()) // Configures the time before which the token cannot be accepted (nbf claim)
            ->setExpiration(time() + 600) // 10 min
            ->set('uid', Yii::$app->user->identity->id) // Configures a new claim, called "uid"
            ->sign($signer,Yii::$app->jwt->key)
            ->getToken(); // Retrieves the generated token

          return ['token'=>(string)$token];
        } else {
            $model->validate();
            return $model;
        }
    }

    /**
  	* Signs user up.
  	*
  	* @return mixed
  	*/
  	public function actionRegistration()
  	{
  		$model = new SignupForm();
  		if ($model->load(Yii::$app->getRequest()->getBodyParams(), '')) {
  			if ($user = $model->signup()) {
  				if (Yii::$app->getUser()->login($user)) {
  					return ['success' => true];
  				}
  			}
  		}
  		return $model;
  	}

}

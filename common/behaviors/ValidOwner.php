<?php

namespace common\behaviors;

use yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\web\HttpException;

class ValidOwner extends Behavior
{
	public function events()
	{
		return [
			ActiveRecord::EVENT_BEFORE_DELETE => 'checkOwner'
		];
	}

	/**
	* Check if current user has access to this record
	*/
	public function checkOwner() {
		$currentUserId = $this->getCurrentUserId();
		$currentCreatorId = $this->owner->creator;
		if($currentCreatorId > 0) {
			if($currentUserId != $currentCreatorId)
			{
				throw new HttpException(403, "Resourse is not allowed");
			}
		}
	}

	private function getCurrentUserId() {
		return Yii::$app->user->identity->id;
	}
}

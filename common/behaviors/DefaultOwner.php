<?php

namespace common\behaviors;

use yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;

class DefaultOwner extends Behavior
{
	public $creator_field = 'creator';
	public function events()
	{
		return [
			ActiveRecord::EVENT_BEFORE_VALIDATE => 'assignOwner'
		];
	}
/**
* Make current user as record owner
*/
	public function assignOwner() {
		$currentUserId = $this->getCurrentUserId();
		if(!is_array($this->creator_field)) {
			$this->creator_field = [$this->creator_field];
		}
		foreach ($this->creator_field as $field) {
			$currentCreatorId = $this->owner->{$field};
			if(empty($currentCreatorId)) {
				$this->owner->{$field} = $currentUserId;
				$this->owner->created_at = time();
			}
		}
	}

	private function getCurrentUserId() {
		return Yii::$app->user->identity->id;
	}
}

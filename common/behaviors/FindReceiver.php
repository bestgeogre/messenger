<?php

namespace common\behaviors;

use yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;

class FindReceiver extends Behavior
{
	public function events()
	{
		return [
			ActiveRecord::EVENT_BEFORE_VALIDATE => 'findReceiver'
		];
	}

	/**
	* Find receiver id by email and assign
	*/
	public function findReceiver() {
		$receiver = \common\models\User::findByEmail($this->owner->email);
		if($receiver) {
			$this->owner->receiver = $receiver->id;
		}
	}
}

<?php

namespace common\models;

use Yii;

/**
 * This is the ActiveQuery class for [[Message]].
 *
 * @see Message
 */
class MessageQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Message[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Message|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * Get own user messages: sent and received
     */
    public function own()
    {
      $user = Yii::$app->user->identity;
      $this->andWhere(['or',['creator' => $user->id], ['receiver' => $user->id]]);
    }
}

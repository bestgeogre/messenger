<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "message".
 *
 * @property int $id
 * @property string $message_title
 * @property string $message_text
 * @property int $created_at
 * @property int $received_at
 * @property int $creator
 * @property int $receiver
 *
 * @property User $creator0
 * @property User $receiver0
 */
class Message extends \yii\db\ActiveRecord
{
    public $email;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'message';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['message_text', 'created_at', 'creator', 'message_title'], 'required'],
            [['message_text','message_title'], 'string'],
            [['created_at', 'received_at', 'creator', 'receiver'], 'integer'],
            [['creator'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['creator' => 'id']],
            [['receiver'], 'exist', 'skipOnEmpty' => false, 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['receiver' => 'id'], 'message' => Yii::t("app","Cannot find receiver")],
            ['email', 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'message_text' => Yii::t('app', 'Text'),
            'message_title' => Yii::t('app', 'Title'),
            'created_at' => Yii::t('app', 'Created At'),
            'received_at' => Yii::t('app', 'Received At'),
            'creator' => Yii::t('app', 'Creator'),
            'receiver' => Yii::t('app', 'Receiver'),
        ];
    }

    public function behaviors()
  	{
  		return [
  			'validateOwner' => [
  				'class' => 'common\behaviors\ValidOwner'
  			],
  			'defaultOwner' => [
  				'class' => 'common\behaviors\DefaultOwner',
  				'creator_field' => 'creator'
  			],
        'findReceiver' => [
          'class' => 'common\behaviors\FindReceiver',
        ]
  		];
  	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator0()
    {
        return $this->hasOne(User::className(), ['id' => 'creator']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReceiver0()
    {
        return $this->hasOne(User::className(), ['id' => 'receiver']);
    }

    /**
     * {@inheritdoc}
     * @return MessageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MessageQuery(get_called_class());
    }
}

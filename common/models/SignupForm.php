<?php
namespace common\models;

use common\models\User;
use yii\base\Model;
use Yii;
use borales\extensions\phoneInput\PhoneInputValidator;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $phone;
    public $email;
    public $password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['phone', 'filter', 'filter' => 'trim'],
            ['phone', 'required'],
            ['phone', 'unique', 'targetClass' => '\common\models\User', 'targetAttribute' => 'username', 'message' => 'This phone has already been taken.'],
            ['phone', PhoneInputValidator::className()],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
		    $cat = "SignupForm:signup";
        if ($this->validate())
        {
          $user = new User();
          $user->username = $this->phone;
          $user->email = $this->email;
          $user->setPassword($this->password);
          $user->generateAuthKey();
          if ($user->save()) {
              return $user;
          }
    			else
    			{
    				Yii::trace("Cannot save user: " . print_r($user->getErrors(),true), $cat);
    			}
        }

        return null;
    }
}

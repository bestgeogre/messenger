Registration
curl -XPOST -H "Content-type: application/json" -d '{"phone":"+380501234567","password":"111111","email":"some@test.com"}' 'http://api.localhost/register'

Authentication
curl -XPOST -H "Content-type: application/json" -d '{"email":"some@test.com","password":"111111"}' 'http://api.localhost/login'

GET MESSAGES
curl -XGET -H 'Authorization: Bearer <token>' -H "Content-type: application/json" 'http://api.localhost/messages[?message_type=sent[&user_status=received]]'

CREATE
curl -XPOST -H 'Authorization: Bearer <token>' -H "Content-type: application/json" -d '{"message_title":"aha","message_title":"ahaha","email":"test1@example.com"}' 'http://api.localhost/messages'

DELETE
curl -XDELETE -H 'Authorization: Bearer <token>' -H "Content-type: application/json" 'http://api.localhost/mesages/5'

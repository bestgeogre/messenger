<?php

use yii\db\Migration;

/**
 * Class m180917_215657_add_records
 */
class m180917_215657_add_records extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->insert('user', [
            'id' => 1,
            'username' => '+380501234567',
            'auth_key' => 'eZiF3FheGaz3frUJh_Q6EvyGiwmeO06p',
            'email' => 'test1@example.com',
            'password_hash' => '$2y$13$aeh35Z4sMe6Ix0MhDX75oeVge2bjImwiQ3bwrc9iEjIvNZPPLlDZe',
            'created_at' => time(),
            'updated_at' => time()
        ]);

      $this->insert('user', [
            'id' => 2,
            'username' => '+380501234568',
            'auth_key' => 'U_0fWPV26VUUEkeHPERIQO5dcqngzoVt',
            'email' => 'test4@example.com',
            'password_hash' => '$2y$13$tMXMOI93kJig1Ea2MSHYY.if4zq/RC9t7OvM5ZWu3fbmdOCtLSzha',
            'created_at' => time(),
            'updated_at' => time()
        ]);

      $this->insert('user', [
            'id' => 3,
            'username' => '+380501234569',
            'auth_key' => '3qJLL-c2HaGV0HkUEERguTQZm0KktvpI',
            'email' => 'test5@example.com',
            'password_hash' => '$2y$13$nOp4g3q5Y7Dckfs9kstfUemPas3MpUGnyVBRQDcUZzbuz3QWoTbVK',
            'created_at' => time(),
            'updated_at' => time()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('user', ['id' => 1]);
    }
}

<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),

            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('{{%message}}', [
            'id' => $this->primaryKey(),
            'message_title' => $this->string()->notNull(),
            'message_text' => $this->text()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'received_at' => $this->integer(),
            'creator' => $this->integer()->notNull(),
            'receiver' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addForeignKey(
              'fk-message-creator',
              'message',
              'creator',
              'user',
              'id',
              'CASCADE'
          );

          $this->addForeignKey(
                'fk-message-receiver',
                'message',
                'receiver',
                'user',
                'id',
                'CASCADE'
            );
    }

    public function down()
    {
      $this->dropForeignKey(
          'fk-message-receiver',
          'message'
      );
      $this->dropForeignKey(
          'fk-message-creator',
          'message'
      );
      $this->dropTable('{{%user}}');
      $this->dropTable('{{%message}}');
    }
}
